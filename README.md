# PHP7 Base Container

This project contains a custom Docker image based on php7-apache. 
Tools like composer (downgraded) and specific php7 lib are added for custom 
development purposes.
