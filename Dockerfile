FROM php:7.2-apache

# System
RUN apt update -qq && apt install -qy

# PHP
RUN apt install -y libicu-dev libpng-dev
RUN docker-php-ext-install intl exif gd zip pdo pdo_mysql

# Composer
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN chmod +x /usr/local/bin/composer
RUN composer self-update 2.2.9
RUN composer --version
